@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Создание Квиза @endslot
            @slot('parent') Главная @endslot
            @slot('active') Квизы @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.quiz.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.quizzes.partials.form')

            <input type="hidden" name="created_by" value="{{Auth::id()}}">
        </form>

    </div>
@endsection