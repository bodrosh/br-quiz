<label for="">Статус</label>
<select class="form-control" name="published">
    @if (isset($quiz->id))
        <option value="0" @if ($quiz->published == 0) selected="" @endif>Не опубликовано</option>
        <option value="1" @if ($quiz->published == 1) selected="" @endif>Опубликовано</option>
    @else
        <option value="0">Не опубликовано</option>
        <option value="1">Опубликовано</option>
    @endif
</select>

<label for="title">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок квиза" value="@if(isset($quiz -> title )){{$quiz -> title}}@endif" required>

<label for="slug">Slug</label>
<input type="text" class="form-control" name="slug" placeholder="Автоматическая генерация" value="@if(isset($quiz -> slug )){{$quiz -> slug}}@endif" readonly="">

<label for="">Описание</label>
<textarea class="form-control" name="description" id="description" placeholder="">@isset($quiz->description ){{$quiz->description}}@endisset</textarea>

<input type="hidden" name="user_id" value="{{Auth::id()}}">

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">
