@extends('admin.layouts.app_admin')

@section('content')

    <div class="content">
        @component('admin.components.breadcrumb')

            @slot('title') Список квизов @endslot
            @slot('parent') Главная @endslot
            @slot('active') Квизы @endslot

        @endcomponent
    </div>


    <a href="{{route('admin.quiz.create')}}" class="btn btn-primary pull-right"> Создать квиз</a>
<hr>
    <table>
        @foreach ($quizzes as $quiz)
            <tr>
                <td>
                    {{$quiz->title}}
                </td>
                <td>
                    |  <a href="{{route('admin.quiz.edit', ['id' => $quiz->id])}}"> Изменить</a>
                </td>
                <td>
                    <form action="{{route('admin.quiz.destroy', $quiz)}}" method="post" onsubmit="if(confirm('Удалить?')) { return true } else { return false }" >
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete" >
                        <button type="submit" class="btn">Удалить</button>
                    </form>
                </td>
                <td>
                    <a href="{{route('admin.question.index', ['quiz_id' => $quiz->id])}}">Вопросы квиза</a>

                </td>
            </tr>
        @endforeach

        <tr>
            <td colspan="2">
                {{$quizzes->links()}}
            </td>
        </tr>

    </table>


@endsection