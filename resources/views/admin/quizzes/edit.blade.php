@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
            @slot('title') Изменение Квиза @endslot
            @slot('parent') Главная @endslot
            @slot('active') Квизы @endslot
        @endcomponent

        <hr>

        <form action="{{route('admin.quiz.update', $quiz)}}" method="post">
            {{ csrf_field() }}
            @include('admin.quizzes.partials.form')

            <input type="hidden" name="_method" value="put" >
        </form>

    </div>
@endsection