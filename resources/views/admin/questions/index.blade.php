@extends('admin.layouts.app_admin')

@section('content')

    <div class="content">
        @component('admin.components.breadcrumb')

            @slot('title') Список вопросов квиза @endslot
            @slot('parent') Главная @endslot
            @slot('active') Вопросы квиза @endslot

        @endcomponent
    </div>

    @isset($quiz )

        <a href="{{route('admin.question.create')}}" class="btn btn-primary pull-right"> Создать вопрос</a>
        <hr>
        <table>
            <thead>
                <tr>
                    <td>Вопрос</td>
                    <td>Тип вопроса</td>
                    <td>Изменить</td>
                    <td>Удалить</td>
                </tr>
            </thead>
            @foreach ($quiz->questions as $question)
                <tr>
                    <td>{{$question->title}}</td>
                    <td>{{$question->question_type->title}}</td>
                    <td>
                        |  <a href="{{route('admin.question.edit', ['id' => $question->id])}}"> Изменить</a>
                    </td>
                    <td>
                        <form action="{{route('admin.question.destroy', $question)}}" method="post" onsubmit="if(confirm('Удалить?')) { return true } else { return false }" >
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete" >
                            <button type="submit" class="btn">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            <tr>
                <td colspan="2">
                    {{$questions->links()}}
                </td>
            </tr>

        </table>


    @else
        Редактирование вопросов напрмую запрещено или данный квиз не найден!
    @endisset




@endsection