@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                {{--<div class="jumbotron">--}}
                    <p><span class="label label-primary">Админ панель</span></p>
                {{--</div>--}}
            </div>
        </div>
        <div class="col-sm-3">
        <div class="jumbotron">
        <p><a href="{{route('admin.quiz.index')}}"><span class="btn btn-block btn-primary">Квизы</span></a></p>
        </div>
        </div>
    </div>
@endsection
