<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // получем квиз вопроса
    public function quiz() {
        return $this->belongsTo('App\Quiz');
    }

    // получем тип вопроса
    public function question_type() {
        return $this->belongsTo('App\QuestionType');
    }
}
