<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str; // для генерации слага

class Quiz extends Model
{
    protected $fillable = ['user_id', 'title', 'description', 'slug', 'published'];

    // делаем слаг
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug( mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmyHi'), '-');
    }

    // связь с вопросами квиза
    public function questions() {
        return $this->hasMany('App\Question');

    }

}
